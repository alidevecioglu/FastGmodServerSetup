@echo off


:checkCMD
if exist steamcmdloc.json (goto readyforUpdate) else (goto createCMD)
:createCMD
echo "SteamCMD Lokasyonunu Buraya Yazin" >> steamcmdloc.json
cls
echo "Lutfen SteamCMD Lokasyonunu steamcmdloc.json Icine Yazip Bu Programi Tekrar Calistiriniz"
pause
exit


cls
:readyforUpdate
SET /p steamcmdloc=<steamcmdloc.json
SET serverFiles=%cd%



echo " ######   ##     ##  #######  ########   ######   ###### "
echo "##    ##  ###   ### ##     ## ##     ## ##    ## ##    ##"
echo "##        #### #### ##     ## ##     ## ##       ##      "
echo "##   #### ## ### ## ##     ## ##     ##  ######   ###### "
echo "##    ##  ##     ## ##     ## ##     ##       ##       ##"
echo "##    ##  ##     ## ##     ## ##     ## ##    ## ##    ##"
echo  "######   ##     ##  #######  ########   ######   ###### "
echo .
echo .
echo "SteamCMD Config Dosyasina Ulasildi SteamCMD Konumu Dogru Ise Update/Yukleme Baslayacaktir"
echo .
echo .
cd %steamcmdloc%
start /wait steamcmd.exe +login anonymous +force_install_dir %serverFiles% +app_update 4020 validate +quit
cls
echo " ######   ##     ##  #######  ########   ######   ###### "
echo "##    ##  ###   ### ##     ## ##     ## ##    ## ##    ##"
echo "##        #### #### ##     ## ##     ## ##       ##      "
echo "##   #### ## ### ## ##     ## ##     ##  ######   ###### "
echo "##    ##  ##     ## ##     ## ##     ##       ##       ##"
echo "##    ##  ##     ## ##     ## ##     ## ##    ## ##    ##"
echo  "######   ##     ##  #######  ########   ######   ###### "
echo .
echo .
echo "Garry's Mod Update/Yukleme Tamamlandi"
echo .
echo .
pause
