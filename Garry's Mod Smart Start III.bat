@echo off

if exist log.txt (goto boot) else (goto logcreate)

:logcreate
echo " ######   ##     ##  #######  ########   ######   ###### " >> log.txt
echo "##    ##  ###   ### ##     ## ##     ## ##    ## ##    ##" >> log.txt
echo "##        #### #### ##     ## ##     ## ##       ##      " >> log.txt
echo "##   #### ## ### ## ##     ## ##     ##  ######   ###### " >> log.txt
echo "##    ##  ##     ## ##     ## ##     ##       ##       ##" >> log.txt
echo "##    ##  ##     ## ##     ## ##     ## ##    ## ##    ##" >> log.txt
echo  "######   ##     ##  #######  ########   ######   ###### " >> log.txt
echo . >> log.txt
echo Log Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
echo . >> log.txt
SET status=LogCreated
goto logfound

:logfound
:checkvars
if exist map.json (goto updatevarsres) else (goto varfilesmaper)
:varfilesplayers
if exist players.json (goto updatevarsres) else (goto varfilesplayerser)
:varfilesworkshop
if exist workshop.json (goto updatevarsres) else (goto varfilesworkshoper)
:varfilesgamemode
if exist gamemode.json (goto updatevarsres) else (goto varfilesgamemodeer)
:varfilescustom
if exist custom.json (goto updatevarsres) else (goto varfilescustomer)
:varfilessteamcmd
if exist steamcmdloc.json (goto updatevarsres) else (goto varfilessteamcmder)
:varfilesserverfiles
if exist serverfiles.json (goto updatevarsres) else (goto varfilesserverfileser)



:varfilesmaper
echo "gm_construct" >> map.json
echo Map Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilesplayers



:varfilesplayerser
echo "16" >> players.json
echo Oyuncu Sayisi Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilesworkshop

:varfilesworkshoper
echo "" >> workshop.json
echo Workshop Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilesgamemode



:varfilesgamemodeer
echo "sandbox" >> gamemode.json
echo Oyunmodu Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilescustom


:varfilescustomer
echo "-disableluarefresh" >> custom.json
echo Custom Baslatma Komutu Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilessteamcmd

:varfilessteamcmder
echo "SteamCMD Lokasyonunu Buraya Yazin" >> steamcmdloc.json
echo SteamCMD Lokasyonu Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto varfilesserverfiles

:varfilesserverfileser
echo "%cd%" >> serverfiles.json
echo Server Dosyalari Lokasyonu Config Dosyasi / %time% / %date% / Tarihinde Olusturuldu >> log.txt
goto updatevarsres

:boot
SET status=readyforStart
:updatevarsres
SET /p map=<map.json
SET /p maxplayers=<players.json
SET /p workshop=<workshop.json
SET /p gamemode=<gamemode.json

SET /p custom=<custom.json
SET /p steamcmdloc=<steamcmdloc.json
SET /p serverfiles=<serverfiles.json
echo Baslatma Komutlari / %time% / %date% / Tarihinde Guncellendi / %map% / %maxplayers% / %workshop% / %gamemode% / %custom% / >> log.txt
goto %status%


cls
:readyforStart
echo " ######   ##     ##  #######  ########   ######   ###### "
echo "##    ##  ###   ### ##     ## ##     ## ##    ## ##    ##"
echo "##        #### #### ##     ## ##     ## ##       ##      "
echo "##   #### ## ### ## ##     ## ##     ##  ######   ###### "
echo "##    ##  ##     ## ##     ## ##     ##       ##       ##"
echo "##    ##  ##     ## ##     ## ##     ## ##    ## ##    ##"
echo  "######   ##     ##  #######  ########   ######   ###### "
echo .
echo .
echo "Server Baslatilmadan Onceki Tum Kontroller Yapidi Server Baslatiliyor"
echo .
echo .
echo "Server " / %time% / %date% / " Tarihinde Baslatildi"
SET status=waitCrash
goto start


:waitCrash
tasklist /fi "imagename eq srcds.exe" |find ":" > nul
if errorlevel 1 goto waitCrash
SET status=CRASHED
goto updatevarsres
:CRASHED
echo Server / %time% / %date% / Tarihinde Coktu Veya Restart Yedi >> log.txt
echo "Server " / %time% / %date% / " Tarihinde Yeniden Baslatildi"
echo .
echo .
goto start














:start
start srcds.exe -console -game garrysmod +map %map% +maxplayers %maxplayers% +host_workshop_collection %workshop% +gamemode %gamemode% %custom%
SET status=waitCrash
goto %status%